class Node:

    global initialValue #initial value of Node 
    initialValue = 0

    #Constructor
    def __init__(self, data=initialValue, children=None):
        self.val = data
        if (children is not None):
            self.children = list(children)
        else:
            self.children = list()
    
    #Purpose - Copy Constructor
    #Input - root of the wanted tree to copy
    #Output - new tree's root
    def copyConstructor(self, rootToCopy):
        root = Node(rootToCopy.val)
        if (rootToCopy.children is None):
            root.children = list()
        else:
            root.children = list()
            for child in rootToCopy.children:
                root.children.append(root.copyConstructor(child))
        return root
        
    

    #Getters:
    def getChildren(self):
        return self.children

    def getFirstChild(self):
        return self.children[0]

    def getLastChild(self):
        return self.children[len(self.children)-1]

    def getChildrenCount(self):
        return len(self.children)

    def getValue(self):
        return self.val

    #Purpose - check if given root is leaf
    #Input - root node
    #Output - boolean answer if the root is leaf
    def isLeaf(self):
        if (len(self.children) == 0):
            return True
        return False


    #Setters:
    #Input- value as number
    def addChild(self, value=initialValue):
        self.children.append(Node(value))

    #Purpose - remove child from children of self
    #Input - child (as Node) to remove
    def removeChild(self, child):
        self.children.remove(child)

    def setValue(self,val):
        self.val = val

    #Purpose - perform prune (left to right) from the received child index (including)
    #Input - first child index to prune from
    def pruneChildrenLTR(self, pruneIndex):
        while pruneIndex < len(self.children):
            self.children.pop(pruneIndex)      # children count is decreasing instead index increase

    #Purpose - perform prune (right to left) from the received child index (including)
    #Input - first child index to prune from
    def pruneChildrenRTL(self, pruneIndex):
        for i in range(pruneIndex):
            self.children.pop(0)     


    #Override Print() using recursive print
    def __str__(self):
        output = f'root: {self.val} \n\t'
        output += self.recPrint()
        return output
    
    #Helper recursive print method
    def recPrint(self, level=0):
        childOutput = ""
        childrenCount = 1
        for child in self.children:
            if (level != 0):
                childOutput += '\t' * level
            if (not child.isLeaf()):        
                childOutput += f'child{childrenCount}: {child.val}\n\t'
                childOutput += child.recPrint(level+1)
            else:
                childOutput += f'child{childrenCount}: {child.val} \n\t'
            childrenCount += 1
        return childOutput
