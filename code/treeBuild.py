import json 
from node import Node

global initialValue #initial value of Node 
initialValue = 0

#Purpose - main function to build a tree from JSON file
#Input- file path to a JSON tree configuration 
#Output - root Node of the tree built from input file
def treeBuild(filepath):
    with open(filepath, 'r') as json_file:
        treeConfig = json.load(json_file)
    treeRoot = Node(initialValue)
    try:
        treeConfig = treeConfig['root']
    except Exception:
        return "Error: couldn't read file"
    fullTree = recBuildTree(treeRoot, treeConfig)
    return fullTree

#Purpose - recursive function to build a tree from JSON file
#Input- root Node, configuration file
#Output - root Node of the tree built from input file
def recBuildTree(root, configFile):
    if (isinstance(root, Node) == False):   #validity check for root's class
        return root
    childrenIndex = 0   
    for key in configFile.keys():
        if (configFile[key] != None and not isinstance(configFile[key], (int, float))):
            root.addChild()
            recBuildTree(root.getChildren()[childrenIndex], configFile[key])

        if (isinstance(configFile[key], (int, float))):
            root.addChild(configFile[key])
        childrenIndex += 1
    return root
