from node import *

#Purpose - performs alpha-beta pruning on a Tree
#Input - Tree as root Node
#Void - this functions changes the tree in the memory
def miniMaxAlgorithm(root):
    global initialValue #initial value of Node 
    initialValue = 0
    if (not isinstance(root, Node)):
        return "Error"
    else:
        rootVal = recMiniMaxAlgorithm(root, True)  # True => root is always Max 
        root.setValue(rootVal)


def recMiniMaxAlgorithm(root, isMax):
    if root.isLeaf():
        return root.val
    else:
        for child in root.getChildren():
            if (isMax):
                maxValue = recMiniMaxAlgorithm(child, not isMax)    #not isMax=>next level is opposite player
                if maxValue > root.getValue() or root.getValue() == initialValue:
                    root.setValue(maxValue)

            else:   #root is Minimum                
                minValue = recMiniMaxAlgorithm(child, not isMax)
                if minValue < root.getValue() or root.getValue() == initialValue:
                    root.setValue(minValue)

    return root.getValue()
