import sys
from node import *

#Purpose - performs alpha-beta pruning on a Tree
#Input - Tree as root Node
#Void - this functions changes the tree in the memory
def alphaBetaPrune(root, LTR):
    global initialValue #initial value of Node 
    initialValue = 0
    if (not isinstance(root, Node)):
        return "Error"
    else:
        alpha = -sys.maxsize
        beta = sys.maxsize
        if (LTR):
            rootVal = recABPruneLTR(root, alpha, beta, True)  # True => root is always Max 
        else:
            rootVal = recABPruneRTL(root, alpha, beta, True) 
        root.setValue(rootVal)

#Purpose - left to right recursive a-b pruning
def recABPruneLTR(root, alpha, beta, isMax):
    if root.isLeaf():
        return root.val
    else:
        for child in root.getChildren():
            if (isMax):
                alphaRet = recABPruneLTR(child, alpha, beta, not isMax)    #not isMax=>next level is opposite player
                if alphaRet > root.getValue() or root.getValue() == initialValue:
                    root.setValue(alphaRet)
                if(alphaRet > alpha):
                        alpha = alphaRet
            else:   #root is Minimum                
                betaRet = recABPruneLTR(child, alpha, beta, not isMax)
                if betaRet < root.getValue() or root.getValue() == initialValue:
                    root.setValue(betaRet)
                if betaRet < beta:
                    beta = betaRet
            if (alpha >= beta): 
                root.pruneChildrenLTR(root.getChildren().index(child)+1) # start index to prune from
    return root.getValue()


#Purpose - right to left recursive a-b pruning
def recABPruneRTL(root, alpha, beta, isMax):
    if root.isLeaf():
        return root.val
    else:
        children = root.getChildren()
        RTLChildren = list(reversed(range(0, root.getChildrenCount())))
        for childIndex in RTLChildren:
            if (isMax):
                alphaRet = recABPruneRTL(children[childIndex], alpha, beta, not isMax)    #not isMax=>next level is opposite player
                if alphaRet > root.getValue() or root.getValue() == initialValue:
                    root.setValue(alphaRet)
                if(alphaRet > alpha):
                        alpha = alphaRet
            else:   #root is Minimum                
                betaRet = recABPruneRTL(children[childIndex], alpha, beta, not isMax)
                if betaRet < root.getValue() or root.getValue() == initialValue:
                    root.setValue(betaRet)
                if betaRet < beta:
                    beta = betaRet
            if (alpha >= beta): 
                root.pruneChildrenRTL(childIndex) # prune children until childIndex
                break
    return root.getValue()
