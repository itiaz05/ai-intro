import os
import random
from treeBuild import treeBuild
from abPrune import alphaBetaPrune
from minMax import miniMaxAlgorithm
from node import *
from os import listdir
from os.path import isfile, join


def __main__():
    jsonsDirPath = 'treesConfigs'   # base path
    userChoice = -1
    structureFiles = [f for f in listdir(jsonsDirPath) if isfile(join(jsonsDirPath, f))]
    while userChoice not in range(len(structureFiles)):    # validity check
        print("Choose tree structure from the list below:")
        for file in structureFiles:
            print(f'To choose {file} press {structureFiles.index(file)}')
        try:
            userChoice = int(input(f'or press {len(structureFiles)} to choose randomly \n'))
        except Exception:
            print("You have entered invalid character, tree will be chosen randomly")
            userChoice = len(structureFiles)
        if userChoice == len(structureFiles):   # random choice
            userChoice = random.randrange(len(structureFiles))
        try:
            structurePath = os.path.join(jsonsDirPath, structureFiles[userChoice])
        except Exception:
            userChoice = -1
            print("Please enter a valid choice")
    try:
        lTRPrune = input("Press 0 to prune from right to left or anything else to prune from left to right \n")
        if (lTRPrune in ['"0"', "'0'", "0", 0]):
            lTRPrune = False
        else:
            lTRPrune = True
    except Exception:
        lTRPrune = True
    root = treeBuild(structurePath)
    print("Tree: \n", root)
    newRoot = root.copyConstructor(root)
    miniMaxAlgorithm(newRoot) 
    print("Tree after MiniMax Algorithm: \n", newRoot)
    alphaBetaPrune(root, lTRPrune)
    print("Tree after LTR A-B Pruning: \n", root) if lTRPrune else print("Tree after RTL A-B Pruning: \n", root)
__main__()



